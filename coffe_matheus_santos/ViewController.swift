//
//  ViewController.swift
//  coffe_matheus_santos
//
//  Created by COTEMIG on 10/11/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Cafe: Decodable {
    let file: String
}

class ViewController: UIViewController {
    
    @IBOutlet var imageView: UIImageView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func recarregarImagem(_ sender: Any) {
        getNovoCafe()
    }
    
    func getNovoCafe(){
        AF.request("https://coffee.alexflipnote.dev/random.json").responseDecodable(of: Cafe.self) {
            response in
            if let cafe = response.value{
                self.imageView.kf.setImage(with: URL(string: cafe.file))
            }
        }
        
    }
    
}

